package com.mehmedean.wifitor;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.mehmedean.wifitor.util.SharedPreferencesUtil;
import com.mehmedean.wifitor.util.Util;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "Wifitor";

    private final static int PERMISSION_CODE_ACCESS_FINE_LOCATION = 0x0001;

    @Bind(R.id.toggle_wifi_button)
    Button mToggleWifiButton;
    @Bind(R.id.connect_wifi_button)
    Button mConnectWifiButton;
    @Bind(R.id.set_wifi_info)
    Button mSetWifiInfoButton;
    @Bind(R.id.wifi_list)
    ListView mWifiListView;
    @Nullable
    @Bind(R.id.wifi_ssid)
    EditText mWifiSsidEditText;
    @Nullable
    @Bind(R.id.wifi_password)
    EditText mWifiPasswordEditText;

    private WifiManager mWifiManager;
    private WifiScanReceiver mWifiScanReceiver;
    private WifiStateReceiver mWifiStateReceiver;
    private IntentFilter mWifiStateIntentFilter;
    private List<ScanResult> mWifiScanList;
    private String[] wifis;
    private int networkId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        mWifiScanReceiver = new WifiScanReceiver();
        mWifiStateReceiver = new WifiStateReceiver();
        mWifiStateIntentFilter = new IntentFilter();
        mWifiStateIntentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        mWifiStateIntentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);

        mToggleWifiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWifiManager.isWifiEnabled()) {
                    mWifiManager.setWifiEnabled(false);
                    mWifiListView.setAdapter(null);
                    wifis = null;
                } else {
                    mWifiManager.setWifiEnabled(true);
                    getWifiList();
                }
            }
        });
        mConnectWifiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mWifiManager.getConnectionInfo().getNetworkId() != -1) {
                    Log.e(TAG, "Disconnect is clicked for " + mWifiManager.getConnectionInfo().getSSID());   // T R Y ! ! !
                    mWifiManager.disconnect();
                } else {
                    Log.e(TAG, "Connect is clicked");    // T R Y ! ! !
                    connect();
                }
            }
        });
        mSetWifiInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String savedWifiSsid = SharedPreferencesUtil.getValue(MainActivity.this, SharedPreferencesUtil.PREFS_KEY_SSID);
                String savedWifiPassword = SharedPreferencesUtil.getValue(MainActivity.this, SharedPreferencesUtil.PREFS_KEY_PASSWORD);
                showWifiInfoDialog(savedWifiSsid, savedWifiPassword);
            }
        });
        mWifiListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedSsid = mWifiScanList.get(position).SSID;
                showWifiInfoDialog(selectedSsid, null);
                return true;
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_CODE_ACCESS_FINE_LOCATION);
            }
        } else {
            getWifiList();
        }
    }

    protected void onPause() {
        unregisterReceiver(mWifiScanReceiver);
        unregisterReceiver(mWifiStateReceiver);
        super.onPause();
    }

    protected void onResume() {
        registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        registerReceiver(mWifiStateReceiver, mWifiStateIntentFilter);
        super.onResume();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_CODE_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getWifiList();
                } else {
                    // Permission denied!
                }
                break;
            }
        }
    }

    private void getWifiList() {
        mWifiManager.startScan();
    }

    private void connect() {
        if (networkId == -1) {
            String ssid = SharedPreferencesUtil.getValue(this, SharedPreferencesUtil.PREFS_KEY_SSID);
            String password = SharedPreferencesUtil.getValue(this, SharedPreferencesUtil.PREFS_KEY_PASSWORD);

            if (ssid != null && password != null) {
                WifiConfiguration conf = new WifiConfiguration();
                conf.SSID = "\"" + ssid + "\"";
                conf.preSharedKey = "\"" + password + "\"";
                networkId = mWifiManager.addNetwork(conf);
            } else {
                Util.showInfoDialog(this, R.string.text_wifi_info_null);
                return;
            }
        }
        Log.e(TAG, "NetworkId: " + networkId);   // T R Y ! ! !
        mWifiManager.disconnect();
        mWifiManager.enableNetwork(networkId, true);
        mWifiManager.reconnect();
    }

    private void showWifiInfoDialog(String ssid, String password) {
        View wifiInfoDialogLayout = View.inflate(MainActivity.this, R.layout.dialog_wifi_info, null);
        mWifiSsidEditText = ButterKnife.findById(wifiInfoDialogLayout, R.id.wifi_ssid);
        mWifiPasswordEditText = ButterKnife.findById(wifiInfoDialogLayout, R.id.wifi_password);

        if (mWifiSsidEditText != null && mWifiPasswordEditText != null) {
            if (password != null) {
                mWifiPasswordEditText.setText(password);
            } else {
                mWifiPasswordEditText.setFocusableInTouchMode(true);
                mWifiPasswordEditText.requestFocus();
            }
            if (ssid != null) {
                mWifiSsidEditText.setText(ssid);
            } else {
                mWifiSsidEditText.setFocusableInTouchMode(true);
                mWifiSsidEditText.requestFocus();
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setView(wifiInfoDialogLayout)
                .setTitle(R.string.set_wifi_info)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (mWifiSsidEditText != null && mWifiPasswordEditText != null) {
                            String wifiSsid = mWifiSsidEditText.getText().toString();
                            String wifiPassword = mWifiPasswordEditText.getText().toString();
                            if (!wifiSsid.isEmpty() && !wifiPassword.isEmpty()) {
                                SharedPreferencesUtil.save(MainActivity.this, SharedPreferencesUtil.PREFS_KEY_SSID, wifiSsid);
                                SharedPreferencesUtil.save(MainActivity.this, SharedPreferencesUtil.PREFS_KEY_PASSWORD, wifiPassword);
                            }
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                }).show();
    }

    private class WifiScanReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            mWifiScanList = mWifiManager.getScanResults();
            wifis = new String[mWifiScanList.size()];

            for (int i = 0; i < mWifiScanList.size(); i++) {
                wifis[i] = ((mWifiScanList.get(i)).toString());
            }
            mWifiListView.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_list_item_1, wifis));
        }
    }

    // WIFI STATES:
    // 0 - WIFI_STATE_DISABLING
    // 1 - WIFI_STATE_DISABLED
    // 2 - WIFI_STATE_ENABLING
    // 3 - WIFI_STATE_ENABLED
    // 4 - WIFI_STATE_UNKNOWN
    //
    // NETWORK STATES:
    // https://developer.android.com/reference/android/net/NetworkInfo.State.html
    private class WifiStateReceiver extends BroadcastReceiver {
        public void onReceive(Context c, Intent intent) {
            String action = intent.getAction();
            // Log.e(TAG, "Action: " + action); // T R Y ! ! !

            if (action.equalsIgnoreCase(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);

                if (wifiState == WifiManager.WIFI_STATE_DISABLED) {
                    Log.e(TAG, "WiFi is disabled"); // T R Y ! ! !
                    mToggleWifiButton.setText(R.string.enable_wifi);
                    mToggleWifiButton.setEnabled(true);
                    mConnectWifiButton.setEnabled(false);
                } else if (wifiState == WifiManager.WIFI_STATE_ENABLED) {
                    Log.e(TAG, "WiFi is enabled");  // T R Y ! ! !
                    mToggleWifiButton.setText(R.string.disable_wifi);
                    mToggleWifiButton.setEnabled(true);
                    mConnectWifiButton.setEnabled(true);
                }
            } else if (action.equalsIgnoreCase(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
                NetworkInfo networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
                Log.i(TAG, "Network State: " + networkInfo.toString());   // T R Y ! ! !

                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    Log.e(TAG, "Connected");    // T R Y ! ! !
                    mConnectWifiButton.setText(R.string.disconnect);
                    mConnectWifiButton.setEnabled(true);
                } else if (networkInfo.getState() == NetworkInfo.State.DISCONNECTED) {
                    Log.e(TAG, "Disconnected");     // T R Y ! ! !
                    mConnectWifiButton.setText(R.string.connect);
                    mConnectWifiButton.setEnabled(true);
                }
            }
        }
    }
}
