package com.mehmedean.wifitor.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public final class SharedPreferencesUtil {
    public static final String PREFS_KEY_SSID = "ssid";
    public static final String PREFS_KEY_PASSWORD = "password";

    public SharedPreferencesUtil() {
        super();
    }

    public static String getValue(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, null);
    }

    public static void save(Context context, String key, String value) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit();
        editor.putString(key, value);
        editor.apply(); // editor.commit();
    }

    public static void removeValue(Context context, String key) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = PreferenceManager.getDefaultSharedPreferences(context);
        editor = settings.edit();
        editor.remove(key);
        editor.apply(); // editor.commit();
    }
}
