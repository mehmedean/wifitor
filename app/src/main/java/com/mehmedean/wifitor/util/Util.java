package com.mehmedean.wifitor.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class Util {
    private static final String TAG = Util.class.getSimpleName();

    public static void showInfoDialog(Context context, int messageId) {
        try {
            String message = context.getResources().getString(messageId);
            showInfoDialog(context, message);
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    public static void showInfoDialog(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public static void showKeyboard(final Context context, final View view) {
        Configuration config = context.getResources().getConfiguration();
        if (config.keyboard == Configuration.KEYBOARD_NOKEYS) { // If device is using soft keyboard
            view.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
                }
            }, 300);
        }
    }
}
